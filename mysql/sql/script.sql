CREATE TABLE usuario (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(250) NOT NULL,
    correo VARCHAR(100) NOT NULL
);

insert into usuario (nombre,correo) values ('docker','docker@tareasa.com');
insert into usuario (nombre,correo) values ('Fabio','fabiodepazv@gmail.com');

